const femme = [
  {
    ref: "jupe_femme",
    category: 0,
    name: "jupe mini",
    price: 19.99,
    unit: "unité",
    image: "jupe.jpg"
  },
  {
    ref: "sac",
    category: 0,
    name: "Sac tendance",
    price: 21.99,
    unit: "unité",
    image: "sac.jpg"
  },
  {
    ref: "jean_femme",
    category: 0,
    name: "Jean taille haute",
    price: 29.99,
    unit: "unité",
    image: "jeanfemme.jpg"
  },
  {
    ref: "kimono_femme",
    category: 0,
    name: "Kimono uni",
    price: 39.99,
    unit: "pièce",
    image: "Kimono.jpg"
  },
  {
    ref: "pantalon_femme",
    category: 0,
    name: "Pantalon",
    price: 39.99,
    unit: "unité",
    image: "pantalon.jpg"
  },
  {
    ref: "robe",
    category: 0,
    name: "Robe",
    price: 5.99,
    unit: "unité",
    image: "robe.jpg"
  },
  ,
  {
    ref: "robe2",
    category: 0,
    name: "Robe",
    price: 24.59,
    unit: "unité",
    image: "robe2.jpg"
  },
  {
    ref: "blouse",
    category: 0,
    name: "Blouse tendance",
    price: 24.99,
    unit: "unité",
    image: "blouse.jpg"
  },
  {
    ref: "blouse2",
    category: 0,
    name: "Chemise dentelle",
    price: 13.49,
    unit: "unité",
    image: "blouse2.jpg"
  }
];  

const homme = [
  {
    ref: "top homme",
    category: 1,
    name: "Tee-shirt",
    price: 19.99,
    unit: "unité",
    image: "top.jpg"
  },
  {
    ref: "top2",
    category: 1,
    name: "Tee-shirt",
    price: 19.99,
    unit: "unité",
    image: "top2.jpg"
  },
  {
    ref: "top homme",
    category: 1,
    name: "Tee-shirt",
    price: 19.99,
    unit: "unité",
    image: "top.jpg"
  },
  {
    ref: "top2",
    category: 1,
    name: "Tee-shirt",
    price: 19.99,
    unit: "unité",
    image: "top2.jpg"
  },
  {
    ref: "short",
    category: 1,
    name: "Short",
    price: 19.99,
    unit: "unité",
    image: "short.jpg"
  },
  {
    ref: "top homme",
    category: 1,
    name: "Tee-shirt",
    price: 19.99,
    unit: "unité",
    image: "top.jpg"
  },
  {
    ref: "short",
    category: 1,
    name: "Short",
    price: 19.99,
    unit: "unité",
    image: "short.jpg"
  },
  {
    ref: "top2",
    category: 1,
    name: "Tee-shirt",
    price: 19.99,
    unit: "unité",
    image: "top2.jpg"
  }
]; 

const womenshoes = [
  {
    ref: "shoes men",
    category: 2,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes.jpg"
  },
  {
    ref: "shoes men",
    category: 2,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  },
  {
    ref: "shoes men",
    category: 2,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes2.jpg"
  },
  {
    ref: "shoes men",
    category: 2,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes3.jpg"
  },
  {
    ref: "shoes men",
    category: 2,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  }
];  

const menshoes = [
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes2.jpg"
  },
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  },
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  },
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  },
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  },
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  },
  {
    ref: "shoes men",
    category: 3,
    name: "basket",
    price: 112.99,
    unit: "pièce",
    image: "shoes4.jpg"
  }
];

const accessoire = [
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  },
  {
    ref: "accessoire",
    category: 4,
    name: "lunette",
    price: 12.99,
    unit: "pièce",
    image: "acc.jpg"
  }
];

export const list = [femme, homme, womenshoes, menshoes, accessoire]